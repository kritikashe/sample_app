class ApplicationMailer < ActionMailer::Base
  default from: "kritika.ongraph@gmail.com"
  layout 'mailer'
end